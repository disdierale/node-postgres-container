'use strict';

const express = require('express');
const Pool = require('pg').Pool

const pool = new Pool({
  user: 'myusername',
  host: 'db',
  database: 'mydbname',
  password: 'toto',
  port: 5432
})

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.get('/info-db', (req,res) => {
    res.status(200).json({
        user: 'myusername',
        host: 'localhost',
        database: 'mydbname',
        password: 'toto',
    })
})

app.get('/db', (req,res) => {
    pool.query('SELECT NOW() as now', (err, res2) => {
      console.log("Je viens de faire une requete à la BD")
      if (err) {
        console.log(err);
        res.send("error")
      } else {
        res.send("Voici le resultat de la requete à la BD : "+res2.rows[0].now);
      }
    })
})

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);